# CSC8427 Preparation

Setting up tools for CSC8427 students

## Installing Software
If you are using a university desktop or laptop computer, then all of the software you will need should already be installed for you.

If, however, you are working on your own personal machine, then you can save time during the first teaching session by downloading and installing the following software:

* The Java Development Kit [(JDK)](https://www.oracle.com/java/technologies/javase-jdk14-downloads.html)
* JetBrains IntelliJ IDEA Integrated Developer Environment (IDE). A number of installation options are available for IntelliJ IDEA.
    * The preferred option for this course is to take advantage of the [Educational License for IntelliJ IDEA](https://www.jetbrains.com/community/education/#students) to install "IntelliJ IDEA Ultimate" edition. Select the "Apply now" option where you will be prompted to apply using your university email address. You will receive an email with a link to accept a licence agreement and to create a JetBrains Account, after which you will be able to download "IntelliJ IDEA Ultimate", along with a number of other of JetBrains' development tools (none of which are needed for this course). The JetBrains licence is renewable for free every year while you retain an academic email address.
        * NOTE: You must only apply for the Educational License if you are NOT going to use your work data and/or code during the course. Using the Educational License for the development of non-course software would be a breach of your licence agreement with JetBrains. Instead, opt for one of the other IntelliJ IDEA editions, or use your own company's IDE.
    * Either IntelliJ IDEA Ultimate or IntelliJ IDEA Community editions are available from [this link](https://www.jetbrains.com/idea/). Select the "Download" option to go to the download page for your operating system. The Ultimate edition is a paid-for product but there is a 30-day trial available, if you are not eligible for an academic licence.
    * You may also try IntelliJ IDEA Edu edition which is available from the Download page, further down.

The JDK and IDE software will be required to complete the course. We will be discussing these tools, how to obtain and install them, and what they are used for during Module 1, so don't worry if you are not able to set up your development environment prior to the course starting.

## Company Policies?
If you (or your company) has a preferred development environment, feel free to use that instead, but be aware that the functionality and appearance may differ from the images/videos used in this course.

## Version Control
### GitLab
This course will use GitLab for version control, located at [https://about.gitlab.com/](https://about.gitlab.com/). You will need to create a new user account on GitLab if you don't already have one. You can register for a free account using the "Login" option on the GitLab landing page, and either registering for a new account of signing in with one of the options listed.

### GitHub Desktop
We will also be using the GitHub Desktop application for managing our connections to our version control repositories. You can download GitHub Desktop for windows and MacOS systems from the [official website](https://desktop.github.com/). Linux users can also use GitHub Desktop, although the Linux version is an unofficial development and is a little more complicated to install. The Linux application is in [this GitHub repository](https://github.com/shiftkey/desktop/releases), and a walkthrough for installation can be found [here](https://dev.to/rahedmir/is-github-desktop-available-for-gnu-linux-4a69).
